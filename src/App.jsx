import { useState, useEffect, Fragment } from "react";

import "./App.css";

const Column = (props) => {
  const { children } = props;
  return <div className="Column">{children}</div>;
};

const Cell = (props) => {
  const { children } = props;
  return <div className="Cell">{children}</div>;
};

function App() {
  const [columns, setColumns] = useState([]);
  const icon = {
    90: "👉",
    180: "👇",
    270: "👈",
    360: "👆",
  };

  const [direction, setDirection] = useState(90);
  const [coordination, setCoordination] = useState({ y: 0, x: 0 });

  useEffect(() => {
    const tmpColumns = [];
    for (let index = 0; index < 10; index++) {
      const cells = [];
      for (let index = 0; index < 10; index++) {
        cells.push(index);
      }
      tmpColumns.push(cells);
    }

    setColumns(tmpColumns);
  }, []);

  const grid = columns.map((column, columnIndex) => {
    return (
      <Column key={columnIndex}>
        {column.map((cell, index) => {
          if (coordination.y === columnIndex && coordination.x === index) {
            return <Cell key={index}>{icon[direction]}</Cell>;
          }
          return <Cell key={index}></Cell>;
        })}
      </Column>
    );
  });

  const moveForward = () => {
    console.log(coordination, direction);
    if (direction === 90) {
      if (coordination.y === 9) {
        setDirection(direction + 90);
      } else {
        setCoordination({ y: coordination.y + 1, x: coordination.x });
      }
    }
    if (direction === 270) {
      if (coordination.y === 0) {
        setDirection(direction + 90);
      } else {
        setCoordination({ y: coordination.y - 1, x: coordination.x });
      }
    }

    if (direction === 180) {
      if (coordination.x === 9) {
        setDirection(direction + 90);
      } else {
        setCoordination({ y: coordination.y, x: coordination.x + 1 });
      }
    }

    if (direction === 360) {
      console.log(coordination, direction);
      if (coordination.x === 0) {
        setDirection(90);
      } else {
        setCoordination({ y: coordination.y, x: coordination.x - 1 });
      }
    }
  };

  const changeDirection = () => {
    if (direction === 360) {
      setDirection(90);
    } else {
      setDirection(direction + 90);
    }
  };

  return (
    <Fragment>
      <div className="controls">
        <button className="button" onClick={changeDirection}>
          Turn right
        </button>
        <button className="button" onClick={moveForward}>
          Move forward
        </button>
        <span className="direction">
          <b>{direction}</b>
        </span>
        <span className="direction">
          <b>{JSON.stringify(coordination)}</b>
        </span>
      </div>
      <div className="Grid">{grid}</div>
    </Fragment>
  );
}

export default App;
